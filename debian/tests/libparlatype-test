#!/bin/sh
# autopkgtest check: Build and run libparlatype's test suite against installed headers

# exit on error
set -e

CFLAGS=`pkg-config --cflags parlatype glib-2.0 gstreamer-1.0 gstreamer-audio-1.0`
LIBS=`pkg-config --libs parlatype glib-2.0 gstreamer-1.0 gstreamer-audio-1.0`
srcdir=`pwd`/libparlatype/tests

# X and D-BUS magic taken from:
# https://piware.de/2013/08/run-autopilot-test-in-autopkgtest/

# start X
(Xvfb :5 >/dev/null 2>&1 &)
XVFB_PID=$!
export DISPLAY=:5

# start local session D-BUS
eval `dbus-launch`
trap "kill $DBUS_SESSION_BUS_PID $XVFB_PID" 0 TERM QUIT INT
export DBUS_SESSION_BUS_ADDRESS
export XAUTHORITY=/dev/null

# build and run tests
for test in player waveloader waveviewer; do
	# Compile tests with -Wno-deprecated-declarations because those
	# warnings go to stderr and make the test fail.
	# This is intended for warnings coming from dependencies, not from
	# parlatype. Note that the test binaries still have to run and succeed.
	gcc -o $test $srcdir/$test.c $srcdir/mock-plugin.c \
		 $CFLAGS $LIBS \
		-DPARLATYPE_COMPILATION \
		-Wno-deprecated-declarations
	G_TEST_SRCDIR=$srcdir ./$test
done
